module.exports = {
    transpileDependencies: ["vuetify"],
    chainWebpack: config => {
        config.module.rules.delete('eslint');
    },
    /*
    Cuando se iniciliaza publicPath como '', funciona correctamente, y muestra la vista de login, pero cuando se inicia
    sesión, se redirigé a localhost:8080 (la página de bienvenida de Wildfly)
    */
    publicPath: process.env.NODE_ENV === 'production' ?
        '/' :
        '/'
};