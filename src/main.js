import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueSession from 'vue-session'
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import vuetify from "@/plugins/vuetify";
import VueToast from 'vue-toast-notification';
import numeral from 'numeral';
import numFormat from 'vue-filter-number-format';
import VueCryptojs from 'vue-cryptojs';
import VCalendar from 'v-calendar';
import VueMoment from 'vue-moment';
import Loading from 'vue-loading-overlay';
import Vue2Editor from "vue2-editor";



import 'vue-toast-notification/dist/theme-sugar.css';

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import 'vue-loading-overlay/dist/vue-loading.css';
import "@/assets/scss/style.scss";
import "@/assets/css/app.css";


Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueSession);
Vue.use(VueToast);
Vue.use(VueCryptojs);
Vue.use(Loading);
Vue.use(Vue2Editor);

//Vue.use(vueSmoothScroll)

Vue.use(VCalendar,{
    componentPrefix: 'vc',  
});
Vue.use(VueMoment);

Vue.filter('numFormat', numFormat(numeral));


new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount("#app");
