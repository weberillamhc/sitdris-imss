import Vue from "vue";
import VueRouter from "vue-router";
import LoginView from "@/views/LoginView";
import { UtilFront } from "@/core";
//import { component } from "vue/types/umd";

Vue.use(VueRouter);

const routes = [
    { path: "/", name: "Login", component: LoginView },
    {
        path: "/dashboard",
        name: "Dashboard",
        component: () =>
            import ("@/views/DashboardView.vue")
    },
    {
        path: "/citas",
        name: "Citas",
        component: () =>
            import ("@/views/AppointmentsView.vue")
    },
    {
        path: "/recepcion",
        name: "Recepcion",
        component: () =>
            import ("@/views/ReceptionView.vue")
    },
    {
        path: "/farmacia",
        name: "Farmacia",
        component: () =>
            import ("@/views/FarmaciaView.vue")
    },
    {
        path: "/consulta",
        name: "Consulta",
        component: () =>
            import ("@/views/MedicalConsultationView.vue")
    },
    {
        path: "/interpretacion",
        name: "Interpretacion",
        component: () =>
            import ("@/views/ImageView.vue")
    },
    {
        path: "/estudio",
        name: "Estudio",
        component: () =>
            import ("@/views/EstudioView.vue")
    },
    {
        path: "/imagen",
        name: "Imagen",
        component: () =>
            import ("@/views/ImagenDashboard.vue")
    },
    {
        path: "/caja",
        name: "Caja",
        component: () =>
            import ("@/views/CajaView.vue")
    }
    ,
    {
        path: "/cuenta",
        name: "Cuenta",
        component: () =>
            import ("@/views/CuentaView.vue")
    }
    ,
    {
        path: "/baseDatos",
        name: "BaseDatos",
        component: () =>
            import ("@/views/BaseDatosView.vue")
    }
    ,
    {
        path: "/usuarios",
        name: "Usuarios",
        component: () =>
            import ("@/views/UsuariosView.vue")
    }
    ,
    {
        path: "/laboratorio",
        name: "Laboratorio",
        component: () =>
            import ("@/views/LaboratorioDashboardView.vue")
    }
    ,
    {
        path: "/pendientesLaboratorio",
        name: "PendientesLaboratorio",
        component: () =>
            import ("@/views/LaboratorioPendienteView.vue")
    }
    ,
    {
        path: "/historicoLaboratorio",
        name: "HistoricoLaboratorio",
        component: () =>
            import ("@/views/LaboratorioHistorialView.vue")
    }
    ,
    {
        path: "/historiaClinica",
        name: "HistoriaClinica",
        component: () =>
            import ("@/views/HistoriaClinicaView.vue")
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    let isAuthenticated = (new Vue).$session.exists();
    if(!isAuthenticated && to.name == 'Login'){
        next();
    }else if(!isAuthenticated && to.name != 'Login'){
        next({ name: 'Login' });
    }else if(isAuthenticated && to.name == 'Dashboard'){
        next();
    }else if(isAuthenticated){
        if(to.name != undefined){
            // let dataUser  =  UtilFront.getDataUser();
            // let withPermission = dataUser.permissions.filter((permission)=>{
            //     return permission.name.toLowerCase() == to.name.toLowerCase();
            // }).length > 0;
            // if(withPermission || UtilFront.withPermission(1) || UtilFront.withPermission(2) || to.name == 'perfil'){
            //     next();
            // }else{
            //     next({ name: 'Dashboard' });
            // }
            next();
        }else{
            next();
        }
        
    }

});

export default router;