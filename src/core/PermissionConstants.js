export default class PermissionConstants {

    static MODIFY_DESTINATION = "Modificar sucursal destino";

    static SUPPLIER_CREATE = "Registrar proveedor";
    static SUPPLIER_MODIFY = "Modificar proveedor";

    static BRANCH_OFFICE_CREATE = "Registrar sucursal";
    static BRANCH_OFFICE_UPDATE = "Modificar sucursal";
    static BRANCH_OFFICE_UPDATE_STATUS = "Modificar estatus sucursal";

    static USER_CREATE = "Registrar usuario";
    static USER_UPDATE = "Modificar usuario";
    static USER_RESET_PASSWORD = "Modificar contraseña usuario";
    static USER_UPDATE_STATUS = "Modificar estatus usuario";

    static CATEGORY_CREATE = "Registrar categoria";
    static CATEGORY_UPDATE = "Modificar categoria";

    static PRODUCT_CREATE = "Registrar producto";
    static PRODUCT_UPDATE = "Modificar producto";

    
    static ROLE_CREATE = "Registrar rol";
    static ROLE_UPDATE = "Modificar rol";

    static LOT_UPDATE = "Modificar lote";

    /* Permisos de Entradas  */
        //Crear entrada
        static REMISION_CREATE = "Crear Remision";
        //Editar entrada
        static REMISION_EDIT = "Editar Remision";
        //Crear pedido
        static REMISION_ORDER = "Crear Pedido";

    /* Permisos de Entradas  */


    /* Permisos de Alamcen */

        //Detalle de stock
        static STOCK_DETAILS = "Detalle de stock";
        //Actualizar Cantidad en stock
        static STOCK_UPDATE_QUANTITY = "Modificar cantidad stock";
        //Transferencias, Transferir cantidad de stock a otra sucursal
        static STOCK_UPDATE_QUANTITY_TRANSFER = "Transferir Stock";
        //Devolucion de stock
        static STOCK_RETURN = "Devolver Stock";
        //Mostrar producto Agotado
        static STOCK_SOLD_OUT = "Producto Agotado";
        //Reasignar stock
        static STOCK_RE_ASING = "Reasignar Producto";

    /* Permisos de Alamcen */


    /* Permisos Orden de Salida */
        //Crear salida
        static ORDER_CREATE = "Crear Envio";
        //Editar orden de salida
        static ORDER_UPDATE = "Editar Envio";



    /* Permisos Orden de Salida */



    /*  PERMISOS DISPENSACIÓN  */

    //Crear una receta
    static PRESCRIPTION_CREATE = "Registrar Dispensación";

    //Actualizar receta
    static PRESCRIPTION_UPDATE = "Actualizar Dispensación";

    //Eliminar receta
    static PRESCRIPTION_DELETE = "Eliminar Dispensación";

    //Mostrar todas las recetas
    static PRESCRIPTION_LIST = "Listar Dispensación";

    //Dispensacion por fecha
    static PRESCRIPTION_BY_DATE = "Dispensación por fecha";










}