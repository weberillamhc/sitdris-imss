import { SECURE_ENDPOINT, UNSECURE_ENDPOINT } from "@/core/AppConfiguration";

export default class ServicePath {
    //**Permission Service*/
    static PERMISSION_LIST_ALL = SECURE_ENDPOINT + "prnsv/prn01";
    static PERMISSION_ADD = SECURE_ENDPOINT + "prnsv/prn02";
    static PERMISSION_UPDATE = SECURE_ENDPOINT + "prnsv/prn03";

    /** Bidding Service */
    static GET_ALL_BIDDINGS = SECURE_ENDPOINT + "bngsvc/bng01";
    static CREATE_BIDDING = SECURE_ENDPOINT + "bngsvc/bng02";
    static UPDATE_BIDDING = SECURE_ENDPOINT + "bngsvc/bng03";
    
    /** Suppler Service */
    static GET_ALL_SUPPLIERS = SECURE_ENDPOINT + "splsvc/spl01";
    static CREATE_SUPPLIER = SECURE_ENDPOINT + "splsvc/spl02";

    /** Remmision Service */
    static GET_REMMISIONS_BY_USER_BRANCH = SECURE_ENDPOINT + "rmsvc/rm01";
    static LIST_ALL_REMMISIONS_BY_BRANCH = SECURE_ENDPOINT + "rmsvc/rm06";
    static CREATE_REMMISION = SECURE_ENDPOINT + "rmsvc/rm02";
    static UPDATE_REMMISION = SECURE_ENDPOINT + "rmsvc/rm03";
    static UPDATE_PRODUCT_REMMISION_BY_STOCK = SECURE_ENDPOINT + "rmsvc/rm04";
    static REMMISIONS_GET_WITH_PRODUCTS_REMMISION = SECURE_ENDPOINT + "rmsvc/rm05";
    static REMMISIONS_GET_WITH_ALL_PRODUCTS_REMMISION = SECURE_ENDPOINT + "rmsvc/rm08";
    static REMMISIONS_ADD_PRODUCT_IN_REMMISION = SECURE_ENDPOINT + "rmsvc/rm09";
    
    /** Financial Source */
    static GET_ALL_FINANCIAL_SOURCES = SECURE_ENDPOINT + "ffsvc/ff01";
    static GET_ALL_FINANCIAL_SOURCES_AVAILABLE = SECURE_ENDPOINT + "ffsvc/ff02";

    /** Products Service */
    static GET_ALL_PRODUCTS = SECURE_ENDPOINT + "prsvc/ps01";
    static GET_ALL_PRODUCTS_AVAILABLE = SECURE_ENDPOINT + "prsvc/ps02";
    static GET_ALL_PRODUCTS_BY_ORDER = SECURE_ENDPOINT + "prsvc/ps07";
    static GET_ALL_PRODUCT_UNIT_MEASUREMENT = SECURE_ENDPOINT + "prsvc/ps09";
    static GET_ALL_PRODUCT_IN_STOCK = SECURE_ENDPOINT + "prsvc/pr11";


    /** Lots Service */
    static GET_LOTS_BY_BRANCH_FISCAL_PRODUCT = SECURE_ENDPOINT + "prmsvc/prm01";
    static GET_ALL_LOTS = SECURE_ENDPOINT + "prmsvc/prm02";


    /** Goverment Requisitions */
    static GET_ALL_GOVREQUISITIONS = SECURE_ENDPOINT + "rqgsvc/rqg05";
    static REQUISITION_LIST_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "rqgsvc/rqg08";
    static CREATE_GOVREQUISITION = SECURE_ENDPOINT + "rqgsvc/rqg02";

    /** Branch Office */
    static GET_ALL_BRANCH_OFFICE_ACTIVE = SECURE_ENDPOINT + "brofsvc/brof02";
    static BRANCH_OFFICE_LIST_ALL_BY_USER = SECURE_ENDPOINT + "brofsvc/brof07";
    static BRANCH_OFFICE_BY_TYPE_AND_DATE = SECURE_ENDPOINT + "brofsvc/brof12";
    static BRANCH_OFFICE_BY_TYPE = SECURE_ENDPOINT + "brofsvc/brof13";

    /** Login Service */
    static SIGNIN_USER = UNSECURE_ENDPOINT + "lsvc/ls01";

    /** Order Service */
    static ORDER_ADD = SECURE_ENDPOINT + "osvc/or01";
    static GET_ALL_ORDER_BY_USER = SECURE_ENDPOINT + "osvc/or03";
    static ORDER_UPDATE = SECURE_ENDPOINT + "osvc/or04";
    static ORDER_BY_BRANCH = SECURE_ENDPOINT + "osvc/or05";
    static ORDER_DETAIL = SECURE_ENDPOINT + "osvc/or06";
    static ORDER_DISPESING = SECURE_ENDPOINT + "osvc/or07";
    static ORDER_HISTORY = SECURE_ENDPOINT + "osvc/or08";
    static ORDER_LIST_ALL_BY_BRANCH_DESTINATION = SECURE_ENDPOINT + "osvc/or09";
    static ORDER_DISPENSING = SECURE_ENDPOINT + "osvc/or10";
    static ORDER_DECREASE = SECURE_ENDPOINT + "osvc/or11";
    static ORDER_ROLL_BACK = SECURE_ENDPOINT + "osvc/or12";

    /** Stock Service */
    static MOVE_STOCK_LOCATION = SECURE_ENDPOINT + "stsvc/stk04";
    static GET_ALL_STOCK_FROM_USER_BRANCH = SECURE_ENDPOINT + "stsvc/stk05";
    static UPDATE_STOCK = SECURE_ENDPOINT + "stsvc/stk07";
    static STOCK_HISTORY_DESTINATION = SECURE_ENDPOINT + "stsvc/stk13";
    static STOCK_HISTORY_ORIGIN = SECURE_ENDPOINT + "stsvc/stk14";
    static STOCK_DELETE_ORDER_STOCK = SECURE_ENDPOINT + "stsvc/stk10";
    static STOCK_DETAIL = SECURE_ENDPOINT + "stsvc/stk11";
    static STOCK_GET_FULL_DETAIL = SECURE_ENDPOINT + "stsvc/stk16";
    static STOCK_ALL_BY_BRANCH_OFFICE_SIMPLE = SECURE_ENDPOINT + "stsvc/stk11";
    static STOCK_LIST_ALL_EXPIRED = SECURE_ENDPOINT + "stsvc/stk17";
    static STOCK_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "stsvc/stk18";
    static STOCK_UPDATE_QUANTITY = SECURE_ENDPOINT + "stsvc/stk19";
    static STOCK_BY_PRODUCTS_AND_BRANCH_OFFICE = SECURE_ENDPOINT + "stsvc/stk20";
    static STOCK_UPDATE_QUANTITY_TRANSFER = SECURE_ENDPOINT + "stsvc/stk21";
    static STOCK_LIST_ALL_TRANSFERS = SECURE_ENDPOINT + "stsvc/stk22";
    
    /** Category Service */
    static CATEGORY_LIST_ALL = SECURE_ENDPOINT + "ctgsvc/ctg01";
    static CATEGORY_ADD = SECURE_ENDPOINT + "ctgsvc/ctg02";
    static CATEGORY_UPDATE = SECURE_ENDPOINT + "ctgsvc/ctg03";

    /** Fiscal found Service*/
    static FISCAL_FUND_LIST_ALL = SECURE_ENDPOINT + "ffsvc/ff01";
    static FISCAL_FUND_ADD = SECURE_ENDPOINT + "ffsvc/ff03";
    static FISCAL_FUND_UPDATE = SECURE_ENDPOINT + "ffsvc/ff04";

    /** Products Service */
    static PRODUCT_LIST_ALL = SECURE_ENDPOINT + "prsvc/ps01";
    static PRODUCT_ADD = SECURE_ENDPOINT + "prsvc/pr03";
    static PRODUCT_UPDATE = SECURE_ENDPOINT + "prsvc/pr04";

    /** Supplier Service */
    static SUPPLIER_LIST_ALL = SECURE_ENDPOINT + "splsvc/spl01";
    static SUPPLIER_ADD = SECURE_ENDPOINT + "splsvc/spl02";
    static SUPPLIER_UPDATE = SECURE_ENDPOINT + "splsvc/spl03";

    /** Shipments Service */
    static SHIPMENT_LIST_ALL_BY_USER = SECURE_ENDPOINT + "shsvc/shp01";
    static SHIPMENT_FIND_DETAIL = SECURE_ENDPOINT + "shsvc/shp02";
    static SHIPMENT_UPDATE = SECURE_ENDPOINT + "shsvc/shp03";
    static ADD_SHIPMENT = SECURE_ENDPOINT + "shsvc/shp04";
    static SHIPMENTS_LIST_BY_PICKING = SECURE_ENDPOINT + "shsvc/shp05";
    static SHIPMENT_FIND_DETAIL_BY_ORDER = SECURE_ENDPOINT + "shsvc/shp06";
    static SHIPMENTS_RETURNS = SECURE_ENDPOINT + "shsvc/shp09";
    static SHIPMENTS_DETAIL_RETURNS = SECURE_ENDPOINT + "shsvc/shp10";
    static SHIPMENT_LIST_ALL_WITH_STATUS = SECURE_ENDPOINT + "shsvc/shp16";
    static SHIPMENT_UPDATE_DESTINATION = SECURE_ENDPOINT + "shsvc/shp17";

    

    /** Jurisdiction Service */
    static JURISDICTION_LIST_ALL = SECURE_ENDPOINT + "jrdsvc/jrd01";
    static JURISDICTION_ADD = SECURE_ENDPOINT + "jrdsvc/jrd02";
    static JURISDICTION_UPDATE = SECURE_ENDPOINT + "jrdsvc/jrd03";

    /** BranchOffice Service */
    static BRANCH_OFFICE_LIST_ALL = SECURE_ENDPOINT + "brofsvc/brof08";
    static BRANCH_OFFICE_LIST_ALL_TYPES = SECURE_ENDPOINT + "brofsvc/brof09";
    static BRANCH_OFFICE_ADD = SECURE_ENDPOINT + "brofsvc/brof01";
    static BRANCH_OFFICE_UPDATE = SECURE_ENDPOINT + "brofsvc/brof03";
    static BRANCH_OFFICE_GET = SECURE_ENDPOINT + "brofsvc/brof10";
    static BRANCH_OFFICE_UPDATE_STATUS = SECURE_ENDPOINT + "brofsvc/brof11";
    static BRANCH_OFFICE_LIST_ALL_STATUS = SECURE_ENDPOINT + "brofsvc/brof02";

    /** User Service */
    static USER_LIST_ALL = SECURE_ENDPOINT + "ussvc/us02";
    static USER_LIST_ALL_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "ussvc/us03";
    static USER_ADD = SECURE_ENDPOINT + "ussvc/us01";
    static USER_UPDATE = SECURE_ENDPOINT + "ussvc/us04";
    static USER_UPDATE_STATATUS = SECURE_ENDPOINT + "ussvc/us05";
    static USER_RESET_PASSWORD = SECURE_ENDPOINT + "ussvc/us06";
    static USER_UPDATE_PASSWORD = SECURE_ENDPOINT + "ussvc/us07";
    static USER_UPDATE_EMAIL = SECURE_ENDPOINT + "ussvc/us08";
    static USER_LIST_ALL_BRANCH_OFFICES = SECURE_ENDPOINT + "brofsvc/brof07";

     /** Rol Service */
     static ROL_LIST_ALL = SECURE_ENDPOINT + "rlsvc/rl01";
     static ROL_ADD = SECURE_ENDPOINT + "rlsvc/rl02";
     static ROL_UPDATE = SECURE_ENDPOINT + "rlsvc/rl03";
     static ROL_LIST_ALL_WITH_PERMMISIONS = SECURE_ENDPOINT + "rlsvc/rl05";
     static ROL_LIST_ALL_PERMMISIONS = SECURE_ENDPOINT + "rlsvc/rl09";

      /** Reception Service */
      static RECEPTION_LIST_ALL_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "repsvc/rep01";
      static RECEPTION_ALL_ORDER_STOCK_BY_SHIPMENT = SECURE_ENDPOINT + "repsvc/rep06";
      static RECEPTION_ALL_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "repsvc/rep07";
      static RECEPTION_GET_WITH_DETAIL = SECURE_ENDPOINT + "repsvc/rep08";
      static RECEPTION_ADD = SECURE_ENDPOINT + "repsvc/rep02";
      static RECEPTION_UPDATE = SECURE_ENDPOINT + "repsvc/";

      /** Reception Service */
      static INCIDENCE_ORDER_ALL_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "inorsvc/inor01";
      static INCIDENTE_ORDER_ALL_BY_BRANCH_OFFICE_FROM_ORDER_STOCK = SECURE_ENDPOINT + "inorsvc/inor02";
      static INCIDENTE_ORDER_UPDATE_STATUS = SECURE_ENDPOINT + "inorsvc/inor03";
      static INCIDENTE_ORDER_ALL_BY_BRANCH_OFFICE_ORIGIN = SECURE_ENDPOINT + "inorsvc/inor04";
      static INCIDENTE_ALL_TYPES = SECURE_ENDPOINT + "inorsvc/inor06";
      static INCIDENTE_ADD = SECURE_ENDPOINT + "inorsvc/inor07";

      /** Return Service */
      static RETURN_LIST_ALL_BY_ORIGIN = SECURE_ENDPOINT + "rtnsvc/rtn01";
      static RETURN_LIST_ALL_BY_DESTINATION = SECURE_ENDPOINT + "rtnsvc/rtn02";
      static RETURN_ADD = SECURE_ENDPOINT + "rtnsvc/rtn03";
      static RETURN_RECEIVE = SECURE_ENDPOINT + "rtnsvc/rtn04";

      /** Doctor Service*/ 
      static DOCTOR_ADD = SECURE_ENDPOINT + "drsvc/dr01";
      static DOCTOR_UPDATE = SECURE_ENDPOINT + "drsvc/dr02";
      static DOCTOR_LIST_ALL_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "drsvc/dr03";

      /** Medical Prescription Service*/ 
      static MEDICAL_PRESCRIPTION_LIST_ALL_BY_BRANCH  = SECURE_ENDPOINT+ "mpsvc/mp01";
      static MEDICAL_PRESCRIPTION_ADD  = SECURE_ENDPOINT+ "mpsvc/mp02";
      static MEDICAL_PRESCRIPTION_BY_PRODUCT  = SECURE_ENDPOINT+ "mpsvc/mp03";
        

      /** Report Service */
      static REPORT_STARTING_ORDER = SECURE_ENDPOINT + "rptsvc/rpt01";
      static REPORT_PRODUCT_ENTRY = SECURE_ENDPOINT + "rptsvc/rpt02";
      static REPORT_RECEPTION = SECURE_ENDPOINT + "rptsvc/rpt03";
      static REPORT_EXPIRED = SECURE_ENDPOINT + "rptsvc/rpt04";
      static REPORT_EXISTENCE = SECURE_ENDPOINT + "rptsvc/rpt05";

      /** Down On Product Service*/ 
      static DOWN_ADD = SECURE_ENDPOINT + "dcsvc/dc01";
      static DOWN_LIST_ALL_BY_BRANCH_AND_USER = SECURE_ENDPOINT + "dcsvc/dc02";


      /* Excel Service */
       static  EXCEL_ORDERS = SECURE_ENDPOINT + "exsvc/ex01";
       static  EXCEL_ORDERS_DISPENSING = SECURE_ENDPOINT + "exsvc/ex02";
       static  EXCEL_ORDERS_DECREASE = SECURE_ENDPOINT + "exsvc/ex03";
       static  EXCEL_ORDERS_PRODUCT_LIST = SECURE_ENDPOINT + "exsvc/ex04";
       static  EXCEL_ORDERS_HISTORICAL_PICKING = SECURE_ENDPOINT + "exsvc/ex05";
       static  EXCEL_ORDERS_MOVEMENT_HISTORY = SECURE_ENDPOINT + "exsvc/ex06"; 
       static  EXCEL_SHIPMENTS  = SECURE_ENDPOINT + "exsvc/ex07";
       static  ffff  = SECURE_ENDPOINT + "exsvc/ex08";
       static  ff  = SECURE_ENDPOINT + "exsvc/ex09";
       static  EXCEL_BRANCH_OFFICE  = SECURE_ENDPOINT + "exsvc/ex09";
       static  EXCEL_USERS  = SECURE_ENDPOINT + "exsvc/ex10";
       static  EXCEL_BIDDINGS  = SECURE_ENDPOINT + "exsvc/ex11";
       static  EXCEL_WAREHOUSE  = SECURE_ENDPOINT + "exsvc/ex12";
       static  EXCEL_DOCTOR  = SECURE_ENDPOINT + "exsvc/ex13";
       static  EXCEL_PRODUCTO  = SECURE_ENDPOINT + "exsvc/ex14";
       static  EXCEL_JURISDICTIONS  = SECURE_ENDPOINT + "exsvc/ex15";
       static  EXCEL_FISCAL_FOUND  = SECURE_ENDPOINT + "exsvc/ex16";
       static  EXCEL_CATEGORY  = SECURE_ENDPOINT + "exsvc/ex17";
       static  EXCEL_ROLE  = SECURE_ENDPOINT + "exsvc/ex18";
       static  EXCEL_SUPPLIERS = SECURE_ENDPOINT + "exsvc/ex20";

       static  EXCEL_ORDERS_DECREASE_DETAIL  = SECURE_ENDPOINT + "exsvc/ex19";
       static  EXCEL_REMMISIONS = SECURE_ENDPOINT + "exsvc/ex21";
       static  EXCEL_REMMISIONS_ORDERS = SECURE_ENDPOINT + "exsvc/ex22";
       static  EXCEL_RECEPTION_PENDINGS = SECURE_ENDPOINT + "exsvc/ex23";
       static  EXCEL_RECEPTION_HISTORY = SECURE_ENDPOINT + "exsvc/ex24";
       static  EXCEL_INCIDENCE = SECURE_ENDPOINT + "exsvc/ex25";
       static  EXCEL_INCIDENCE_IN = SECURE_ENDPOINT + "exsvc/ex26";
       static  EXCEL_DASHBOARD_BRANCH_OFFICE = SECURE_ENDPOINT + "exsvc/ex27";



       static  EXCEL_MOVEMENTS_INVENTORY_STOCK  = SECURE_ENDPOINT + "exsvc/ex98";
       static  EXCEL_MOVEMENTS_STOCK  = SECURE_ENDPOINT + "exsvc/ex99";
       static EXCEL_CARDEX = SECURE_ENDPOINT + "exsvc/ex100";
       static EXCEL_MEDICAL_PRESCRIPTION_BY_DATE = SECURE_ENDPOINT + "exsvc/ex101";
       static EXCEL_TRANFER = SECURE_ENDPOINT + "exsvc/ex102";
       static EXCEL_RETURN = SECURE_ENDPOINT + "exsvc/ex103";




      /** DashBoard Service*/ 
      static DASHBOARD_DETAIL = SECURE_ENDPOINT + "dasvc/da01";
      static DASHBOARD_PERCENTAGE_BY_BRANCH_OFFICE = SECURE_ENDPOINT + "dasvc/da02";
      static DASHBOARD_BRANCHOFFICE_QUANTITIES = SECURE_ENDPOINT + "dasvc/da04";


      // SITDRIS 
    //**Patient service */
    static SITDRIS_PATH = "http://187.188.98.157:8084/ws-sitdris/";

    static PATIENT_LISTALL = this.SITDRIS_PATH + "patient/listPatients";
    static PATIENT_LISTALL_WITH_STUDIES = this.SITDRIS_PATH + "patient/listPatientsWithStudies";
    static PATIENT_ADD = this.SITDRIS_PATH + "patient/save";
    static PATIENT_APPOINTMENTS = this.SITDRIS_PATH + "patient/appointmentsByPatient";
    static APPOINTMENT_ADD = this.SITDRIS_PATH + "appointment/save";
    static APPOINTMENT_GET = this.SITDRIS_PATH + "appointment/getAppointment";
    static APPOINTMENT_UPDATE = this.SITDRIS_PATH + "appointment/update";
    static APPOINTMENT_CANCEL = this.SITDRIS_PATH + "appointment/cancel";
    static APPOINTMENT_REPROGRAMMING = this.SITDRIS_PATH + "appointment/reprogramming";
    static APPOINTMENT_LISTALL = this.SITDRIS_PATH + "appointment/listAppointments";
    static  IMAGE_ADD= this.SITDRIS_PATH + "image/save";
    static  IMAGE_LISTALL= this.SITDRIS_PATH + "image/listStudies";

}