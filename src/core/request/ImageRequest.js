/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

 import axios from "axios";
 import { ServicePath } from "@/core";
 
 export default class ImageRequest {

     static listAll() {
         return axios.post(ServicePath.IMAGE_LISTALL);
     }
     static add(object) {
         return axios.post(ServicePath.IMAGE_ADD, object);
     }
 }