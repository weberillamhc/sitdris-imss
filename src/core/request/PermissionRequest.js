/**
 * @author DIEGO GS
 * @company Erillam Healthcare
 * @version 1.0
 */

 import axios from "axios";
 import { ServicePath,UtilFront } from "@/core";
 
 export default class PermissionRequest {
     static listAll() {
         return axios.post(ServicePath.PERMISSION_LIST_ALL);
     }
     static add(object) {
         return axios.post(ServicePath.PERMISSION_ADD, UtilFront.setUserID(object));
     }
     static update(object) {
         return axios.post(ServicePath.PERMISSION_UPDATE, UtilFront.setUserID(object));
     }
 }