/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class UserRequest {
    static listAll() {
        return axios.post(ServicePath.USER_LIST_ALL);
    }
    static listAllByBranchOffice(idBranchOffice) {
        return axios.post(ServicePath.USER_LIST_ALL_BY_BRANCH_OFFICE, {id: idBranchOffice});
    }
    static add(object) {
        object["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.USER_ADD, object);
    }
    static update(object) {
        object["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.USER_UPDATE, object);
    }
    static updateStatus(object) {
        object["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.USER_UPDATE_STATATUS, object);
    }
    static updatePassword(object) {
        object["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.USER_UPDATE_PASSWORD, object);
    }
    static updateEmail(object) {
        object["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.USER_UPDATE_EMAIL, object);
    }
    static listAllBranchOfficesByUserId(idUser) {
        let params = {id: idUser, userID: UtilFront.getIdUser()}
        return axios.post(ServicePath.USER_LIST_ALL_BRANCH_OFFICES, params);
    }
    static resetPassword(idUser) {
        let params = {id: idUser, userID: UtilFront.getIdUser()}
        return axios.post(ServicePath.USER_RESET_PASSWORD, params);
    }
}