/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class DashboardRequest {

    static dashboardDetail(data) {
        return axios.post(ServicePath.DASHBOARD_DETAIL,data);
    }

    static dashboardPercentage(data) {
        return axios.post(ServicePath.DASHBOARD_PERCENTAGE_BY_BRANCH_OFFICE,data);
    }

    static dashboardBranOfficeQuantities(){
        return axios.post(ServicePath.DASHBOARD_BRANCHOFFICE_QUANTITIES);
    }

}