/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";


export default class BranchOfficeRequest {

    static listAllActive() {
        return axios.post(ServicePath.GET_ALL_BRANCH_OFFICE_ACTIVE);
    }
    static listAll() {
        return axios.post(ServicePath.BRANCH_OFFICE_LIST_ALL);
    }
    static add(object) {
        object["idUser"] = UtilFront.getIdUser();
        return axios.post(ServicePath.BRANCH_OFFICE_ADD, object);
    }
    static update(object) {
        object["idUser"] = UtilFront.getIdUser();
        return axios.post(ServicePath.BRANCH_OFFICE_UPDATE, object);
    }
    static listAllTypes() {
        return axios.post(ServicePath.BRANCH_OFFICE_LIST_ALL_TYPES);
    }
    static get(object) {
        return axios.post(ServicePath.BRANCH_OFFICE_GET, object);
    }
    static updateStatus(object) {
        object["idUser"] = UtilFront.getIdUser();
        return axios.post(ServicePath.BRANCH_OFFICE_UPDATE_STATUS, object);
    }
    static listAllByUser(object) {
        return axios.post(ServicePath.BRANCH_OFFICE_LIST_ALL_BY_USER, object);
    }

    static listAllStatusActive() {
      return axios.post(ServicePath.BRANCH_OFFICE_LIST_ALL_STATUS);
    }

    static listAllByUserKey(object) {
        let branchOfficeArray = [];

        this.listAllByUser(object).then(response => {
          for (var [index, value] of Object.entries(response.data.data.objects)) {
            let branchOfficeItem = {
              text: value.name,
              value: value.idBranchoffice
            };
            branchOfficeArray.push(branchOfficeItem);
          }
        });
        //console.log(branchOfficeArray);
        return branchOfficeArray;
    }

    static listAllKey( id ) {
        let branchOfficeArray = [];

        return new Promise((resolve, reject) => {

          this.listAllStatusActive().then(response => {
            for (var [index, value] of Object.entries(response.data.data)) {
              if ( id != value.idBranchoffice) {
                let branchOfficeItem = {
                  text: value.name + " | " + value.branchOfficeKey ,
                  value: value.idBranchoffice
                };
                branchOfficeArray.push(branchOfficeItem);
              }
            }
    
            resolve(branchOfficeArray); 
    
          }).catch( (error) => {
            reject(error);
          });
    
        });

    }


    static listAllByTypeAndDate(data) {
        return axios.post(ServicePath.BRANCH_OFFICE_BY_TYPE_AND_DATE,data);
    }

    static listAllByType() {
      return axios.post(ServicePath.BRANCH_OFFICE_BY_TYPE);
    }

    static listAllByTypeKey() {
      let branchOfficeArray = [];
      this.listAllByType().then(response => {
        for (var [index, value] of Object.entries(response.data.data.objects)) {
          let branchOfficeItem = {
            text: value.name,
            value: value.idBranchoffice
          };
          branchOfficeArray.push(branchOfficeItem);
        }
      });
      return branchOfficeArray;
  }



}