/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

 import axios from "axios";
 import { ServicePath } from "@/core";
 
 export default class PatientRequest {
     static listAll() {
         return axios.post(ServicePath.PATIENT_LISTALL);
     }
     static listAllWithStudies() {
         return axios.post(ServicePath.PATIENT_LISTALL_WITH_STUDIES);
     }
     static add(object) {
         return axios.post(ServicePath.PATIENT_ADD, object);
     }
     static update(object) {
         return axios.post(ServicePath.ROL_UPDATE, object);
     }
     static findAllAppointMents(idPatient) {
         return axios.post(ServicePath.PATIENT_APPOINTMENTS, {idPatient: idPatient});
     }
 }