/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class DoctorRequest {

    static addDoctor(data) {
        return axios.post(ServicePath.DOCTOR_ADD,UtilFront.setUserID(data));
    }

    static updateDoctor(data) {
        return axios.post(ServicePath.DOCTOR_UPDATE,data);
    }

    static findAllDoctorByBranchOffice(data) {
        return axios.post(ServicePath.DOCTOR_LIST_ALL_BY_BRANCH_OFFICE,data);
    }

    static findAllDoctorByBranchOfficeKey(data) {
        let doctorArray = [];
        this.findAllDoctorByBranchOffice(data).then(response => {

          for (var [index, value] of Object.entries(response.data.data.objects)) {
            let doctoritem = {
                idDoctor: value.idDoctor,
                name: value.name,
                lastName: value.lastName,
                professionalId: value.professionalId
            };
            doctorArray.push(doctoritem);
          }
        });
        return doctorArray;
    }

    static findAllDoctorByBranchOfficeKeyAvailable(data) {
        let doctorArray = [];
        this.findAllDoctorByBranchOffice(data).then(response => {
          for (var [index, value] of Object.entries(response.data.data.objects)) {
        
            let doctoritem = {
              text: value.name + " " + value.lastName + " | " + value.professionalId,
              value: value.idDoctor
            };
            doctorArray.push(doctoritem);
          }
        });
        return doctorArray;
      }

}