/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath,UtilFront } from "@/core";

export default class ProductsRequest {
    static listAll() {
        return axios.post(ServicePath.PRODUCT_LIST_ALL);
    }
    static add(object) {
        return axios.post(ServicePath.PRODUCT_ADD, UtilFront.setUserID(object));
    }
    static update(object) {
        return axios.post(ServicePath.PRODUCT_UPDATE, UtilFront.setUserID(object));
    }
    static listAllUnitMeasurement() {
        return axios.post(ServicePath.GET_ALL_PRODUCT_UNIT_MEASUREMENT);
    }
}