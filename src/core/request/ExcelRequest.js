/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class ExcelRequest {

    static responseTypeV = {responseType: 'blob'};

    static download( response, fileName ){
      const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/vnd.ms-excel'
      }))

      /* Simulo el click */
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
    }

    static reportOrders(data) {
      return axios.post( ServicePath.EXCEL_ORDERS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "ordenesSalida.xlsx" );
      });
    }

    static reportOrdersDispensing(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_DISPENSING, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "dispensaciones.xlsx" );
      });
    }

    static reportOrdersDecrease(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_DECREASE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "mermas.xlsx" );
      });
    }

    static reportOrdersProductsList(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_PRODUCT_LIST, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "listaProductos.xlsx" );
      });
    }

    static reportOrdersHistoricalPicking(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_HISTORICAL_PICKING, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "historialPicking.xlsx" );
      });
    }

    static reportOrdersMovementHistory(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_MOVEMENT_HISTORY, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "movimientoPicking.xlsx" );
      });
    }

    static reportShipments(data) {
      return axios.post( ServicePath.EXCEL_SHIPMENTS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "enviosHistorial.xlsx" );
      });
    }

    static reportBranchOffice(data) {
      return axios.post( ServicePath.EXCEL_BRANCH_OFFICE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "sucursales.xlsx" );
      });
    }

    static reportUsers(data) {
      return axios.post( ServicePath.EXCEL_USERS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "usuario.xlsx" );
      });
    }

    static reportBiddings(data) {
      return axios.post( ServicePath.EXCEL_BIDDINGS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "licitaciones.xlsx" );
      });
    }

    static reportWareHouse(data) {
      return axios.post( ServicePath.EXCEL_WAREHOUSE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "almacen.xlsx" );
      });
    }

    /* Categorias */
    static reportDoctor(data) {
      return axios.post( ServicePath.EXCEL_DOCTOR, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "doctores.xlsx" );
      });
    }

    static reportProducts(data) {
      return axios.post( ServicePath.EXCEL_PRODUCTO, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "productos.xlsx" );
      });
    }

    static reportjurisdictions(data) {
      return axios.post( ServicePath.EXCEL_JURISDICTIONS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "jurisdiccion.xlsx" );
      });
    }

    static reportFiscalFound(data) {
      return axios.post( ServicePath.EXCEL_FISCAL_FOUND, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "fuenteFinanciamiento.xlsx" );
      });
    }

    static reportCategory(data) {
      return axios.post( ServicePath.EXCEL_CATEGORY, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "categorias.xlsx" );
      });
    }

    static reportRole(data) {
      return axios.post( ServicePath.EXCEL_ROLE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "roles.xlsx" );
      });
    }

    static reportSupplier(data) {
      return axios.post( ServicePath.EXCEL_SUPPLIERS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "proveedor.xlsx" );
      });
    }
    /*Category*/ 

    static reportOrderDecreaseDetail(data) {
      return axios.post( ServicePath.EXCEL_ORDERS_DECREASE_DETAIL, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "mermaDetalle.xlsx" );
      });
    }

    static reportRemmisions(data) {
      return axios.post( ServicePath.EXCEL_REMMISIONS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "entradas.xlsx" );
      });
    }

    static reportRemmisionsOrder(data) {
      return axios.post( ServicePath.EXCEL_REMMISIONS_ORDERS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "pedidos.xlsx" );
      });
    }


    static reportReceptionPending(data) {
      return axios.post( ServicePath.EXCEL_RECEPTION_PENDINGS, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "recepcionesPendientes.xlsx" );
      });
    }

    static reportReceptionHistory(data) {
      return axios.post( ServicePath.EXCEL_RECEPTION_HISTORY, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "recepcionesHistorico.xlsx" );
      });
    }


    static reportIncidence(data) {
      return axios.post( ServicePath.EXCEL_INCIDENCE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "incidenciasRealizadas.xlsx" );
      });
    }


    
    static reportIncidenceIn(data) {
      return axios.post( ServicePath.EXCEL_INCIDENCE_IN, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "incidenciasEntrantes.xlsx" );
      });
    }

    static reportOrderPdf(data) {
      return axios.post( ServicePath.REPORT_STARTING_ORDER, data,this.responseTypeV
      ).then( (response) => {

      const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/pdf'
      }))

      window.open(url);

      });

    }
    static reportReceptionPdf(data) {
      return axios.post( ServicePath.REPORT_RECEPTION, data,this.responseTypeV
      ).then( (response) => {

      const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/pdf'
      }))

      window.open(url);

      });

    }
    
    static downloadPDFExistence(data) {
      return axios.post( ServicePath.REPORT_EXISTENCE, data,this.responseTypeV);
    }

    static reportRemmisionPdf(data) {
      return axios.post(ServicePath.REPORT_PRODUCT_ENTRY, data,this.responseTypeV
      ).then( (response) => {

      const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/pdf'
      }))

      window.open(url);

      });

    }

    static reportExpired(data) {
      return axios.post( ServicePath.REPORT_EXPIRED, data,this.responseTypeV)
    }

    static reportTraceabilityProduct(data) {
      return axios.post( ServicePath.EXCEL_CARDEX, data , this.responseTypeV);
    }













    


    /* Movimientos y existencias */
    static reportMovementsStock(data) {
      return axios.post( ServicePath.EXCEL_MOVEMENTS_STOCK, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "movimientoExistencias.xlsx" );
      });
    }

    static reportMovementsInventoryStock(data) {
      return axios.post( ServicePath.EXCEL_MOVEMENTS_INVENTORY_STOCK, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "movimientoInventarioExistencias.xlsx" );
      });
    }


    static reportMedicalPrescriptionByDate(data) {
      return axios.post( ServicePath.EXCEL_MEDICAL_PRESCRIPTION_BY_DATE, data , this.responseTypeV);
    }


    /* Dashboard */
    static dashboard(path, sucursal) {
      return axios.post( path, this.responseTypeV
      ).then( (response) => {
        this.download( response, sucursal+".xlsx" );
      });
    }

    static reportDashboardBranchOffice(data) {
      return axios.post( ServicePath.EXCEL_DASHBOARD_BRANCH_OFFICE, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "porcentajeSucursal.xlsx" );
      });
    }

    static reportTransfer(data) {
      return axios.post( ServicePath.EXCEL_TRANFER, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "transferencias.xlsx" );
      });
    }

    static reportReturn(data) {
      return axios.post( ServicePath.EXCEL_RETURN, data , this.responseTypeV
      ).then( (response) => {
        this.download( response, "devoluciones.xlsx" );
      });
    }





}