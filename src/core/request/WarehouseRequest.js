/**
 * @author Ivo Danic Garrido
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class WarehouseRequest {
  responseData = [];
  static findAllStockWharehouse(id) {
    return axios.post(ServicePath.GET_ALL_STOCK_FROM_USER_BRANCH, {
      idUser: id
    });
  }
}
