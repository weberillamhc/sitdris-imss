/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class StockRequest {

    static deleteOrderStock(data) {
        data["userID"] = UtilFront.getIdUser();
        return axios.post(ServicePath.STOCK_DELETE_ORDER_STOCK,data);
    }

    static updateStockQuantity(data) {
        return axios.post(ServicePath.STOCK_UPDATE_QUANTITY,data);
    }

    static updateStockQuantityTransfer(data) {
        return axios.post(ServicePath.STOCK_UPDATE_QUANTITY_TRANSFER,data);
    }

    static listAllTransfer(data) {
        return axios.post(ServicePath.STOCK_LIST_ALL_TRANSFERS,data);
    }

    static listAllStockWithExistenceByBranchOffice(idBranchOffice) {
        let params = { idUser: null, idBranchOffice: idBranchOffice };
        return axios.post(ServicePath.GET_ALL_STOCK_FROM_USER_BRANCH, params);
    }

    static listAllStockByProductAndBranchOffice(data) {
        return axios.post(ServicePath.STOCK_BY_PRODUCTS_AND_BRANCH_OFFICE, data);
    }
    
    static getDetailFull(idStock) {
        return axios.post(ServicePath.STOCK_GET_FULL_DETAIL, {id:idStock});
    }

    static listAllStockExpired(idBranchOffice, showAll) {
        let params = { userID: UtilFront.getIdUser(), id: idBranchOffice, showAll: showAll };
        return axios.post(ServicePath.STOCK_LIST_ALL_EXPIRED, params);
    }


}