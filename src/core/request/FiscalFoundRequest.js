/**
 * @author Ivo Danic Garrido
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class FiscalFoundRequest {
  static findAllFiscalFounds() {
    return axios.post(ServicePath.GET_ALL_FINANCIAL_SOURCES);
  }
  static findAllFiscalFoundsAvailable(data) {
    return axios.post(ServicePath.GET_ALL_FINANCIAL_SOURCES_AVAILABLE,data);
  }

  static findAllFiscalFoundsKey() {
    let fiscalFoundArray = [];
    this.findAllFiscalFounds().then(response => {
      for (var [index, value] of Object.entries(response.data.data.objects)) {
        index;
        let fiscalFoundItem = {
          text: value.key,
          value: value.idFiscalfund
        };
        fiscalFoundArray.push(fiscalFoundItem);
      }
    });
    return fiscalFoundArray;
  }

  static findAllFiscalFoundsKeyAvailable(data) {
    let fiscalFoundArray = [];
    this.findAllFiscalFoundsAvailable(data).then(response => {
      for (var [index, value] of Object.entries(response.data.data.objects)) {
        index;
        let fiscalFoundItem = {
          text: value.key,
          value: value.idFiscalfund
        };
        fiscalFoundArray.push(fiscalFoundItem);
      }
    });
    return fiscalFoundArray;
  }
}
