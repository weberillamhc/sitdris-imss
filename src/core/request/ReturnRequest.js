/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class ReturnRequest {

    static listAllByOrigin(idBranchOffcie) {
        return axios.post(ServicePath.RETURN_LIST_ALL_BY_ORIGIN, {id:idBranchOffcie});
    }
    static listAllByDestination(idBranchOffcie) {
        return axios.post(ServicePath.RETURN_LIST_ALL_BY_DESTINATION, {id:idBranchOffcie});
    }
    static add(object) {
        return axios.post(ServicePath.RETURN_ADD, object);
    }
    static recive(object) {
        return axios.post(ServicePath.RETURN_RECEIVE, object);
    }
    
}