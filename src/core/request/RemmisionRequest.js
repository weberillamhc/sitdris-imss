/**
 * @author Alan Saucedo Colin
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class RemmisionRequest {

  static listAllRemmision(data) {
    return axios.post(ServicePath.REMMISIONS_GET_WITH_ALL_PRODUCTS_REMMISION, data);
  }

  static updateProductRemmision(data) {
    return axios.post(ServicePath.UPDATE_PRODUCT_REMMISION_BY_STOCK, data);
  }

  static updateRemmision(data) {
    return axios.post(ServicePath.UPDATE_REMMISION, data);
  }

  static addProductInRemmision(data) {
    return axios.post(ServicePath.REMMISIONS_ADD_PRODUCT_IN_REMMISION, data);
  }

}
