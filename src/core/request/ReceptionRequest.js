/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class ReceptionRequest {
    static listAllByBranchOffice(idBranchOffcie) {
        return axios.post(ServicePath.RECEPTION_LIST_ALL_BY_BRANCH_OFFICE, {id:idBranchOffcie});
    }
    static listAllOrderStockByShipment(idShipment) {
        return axios.post(ServicePath.RECEPTION_ALL_ORDER_STOCK_BY_SHIPMENT, {id:idShipment});
    }
    static listAllByBranchOfficeHistory(idBranchOffcie) {
        return axios.post(ServicePath.RECEPTION_ALL_BY_BRANCH_OFFICE, {id: idBranchOffcie});
    }
    static getDetailById(idReception) {
        return axios.post(ServicePath.RECEPTION_GET_WITH_DETAIL, {id: idReception});
    }
    static add(object) {
        return axios.post(ServicePath.RECEPTION_ADD, object);
    }
    static update(object) {
        return axios.post(ServicePath.USER_UPDATE, object);
    }
    
}