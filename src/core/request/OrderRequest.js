/**
 * @author Ivo Danic Garrido
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath, UtilFront } from "@/core";

export default class OrderRequest {

  static addOrder(data) {
    return axios.post(ServicePath.ORDER_ADD, data);
  }

  static updateOrder(data) {
    return axios.post(ServicePath.ORDER_UPDATE, data);
  } 

  static orderDetail(data) {
    return axios.post(ServicePath.ORDER_DETAIL, data);
  } 

  static orderDispensing(data) {
    return axios.post(ServicePath.ORDER_DISPESING, data);
  } 



  static findAllOrderByBranch(data) {
    return axios.post(ServicePath.ORDER_BY_BRANCH, data);
  }

  static findLotsByFFUser(data) {
    return axios.post(ServicePath.GET_ALL_STOCK_FROM_USER_BRANCH, data);
  }

  static updateStockByOrder(data) {
      data.idUser = UtilFront.getIdUser;
      return axios.post(ServicePath.UPDATE_STOCK, data);
  }

  static findAllOrdersProductByUser(data) {
    return axios.post(ServicePath.UPDATE_STOCK, data);
  }

  static findAllOrderByUser(data) {
    return axios.post(ServicePath.GET_ALL_ORDER_BY_USER, data);
  }

  static orderHistory(data) {
    return axios.post(ServicePath.ORDER_HISTORY, data);
  }

  static orderDispensingHistory(data) {
    return axios.post(ServicePath.ORDER_DISPENSING, data);
  }

  static orderDecrease(data) {
    return axios.post(ServicePath.ORDER_DECREASE, data);
  }

  static orderRollBack(data) {
    return axios.post(ServicePath.ORDER_ROLL_BACK, data);
  }

  
  static findAllOrderByUserKey(data) {
    let ordersItemArray = [];
    this.findAllOrderByUser(data).then(response => {
      for (var [index, value] of Object.entries(response.data.data.objects)) {
        let productItem = {
          orderId: value.orderId,
          shipmentsId: value.shipmentsId,
          destination: value.destination,
          destinationId: value.destinationId,
          date: value.date,
          generate: value.orderUser,
          status: value.status,
          statusShipmentValue: value.statusShipmentValue,
          destinationKey: value.destinationKey
        };
        ordersItemArray.push(productItem);
      }
    });
    return ordersItemArray;
  }

  static findAllOrderByBranchKey(data) {
    let ordersItemArray = [];
    this.findAllOrderByBranch(data).then(response => {
      for (var [index, value] of Object.entries(response.data.data.objects)) {
        let productItem = {
          orderId: value.orderId,
          clues: value.clues,
          destination: value.destination,
          picking: value.picking,
          validator: value.validator,
          validatorDate: value.validatorDate,
          pickingDate: value.pickingDate,
        };
        ordersItemArray.push(productItem);
      }
    });
    return ordersItemArray;
  }


  static listAllOrdersByBranchOfficeDestination(idBranchOffice) {
    return axios.post(ServicePath.ORDER_LIST_ALL_BY_BRANCH_DESTINATION, {id:idBranchOffice});
  }

}
