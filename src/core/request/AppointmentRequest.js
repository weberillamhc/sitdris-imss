/**
 * @author Fernando FH
 * @company Erillam Healthcare
 * @version 1.0
 */

 import axios from "axios";
 import { ServicePath } from "@/core";
 
 export default class AppointmentRequest {
     static listAll() {
         return axios.post(ServicePath.APPOINTMENT_LISTALL);
     }
     static add(object) {
         return axios.post(ServicePath.APPOINTMENT_ADD, object);
     }
     static get(object) {
        return axios.post(ServicePath.APPOINTMENT_GET, object);
    }
     static update(object) {
         return axios.post(ServicePath.APPOINTMENT_UPDATE, object);
     }
     static cancel(object) {
        return axios.post(ServicePath.APPOINTMENT_CANCEL, object);
    }
     static reprogramming(object) {
        return axios.post(ServicePath.APPOINTMENT_REPROGRAMMING, object);
    }
 }