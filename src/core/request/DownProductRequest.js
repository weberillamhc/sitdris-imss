/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class DownProductRequest {

    static addDown(data) {
        return axios.post(ServicePath.DOWN_ADD,data);
    }

    static findAllBranchAndUser(data) {
        return axios.post(ServicePath.DOWN_LIST_ALL_BY_BRANCH_AND_USER,data);
    }


    static findAllBranchAndUserKey(data) {
        let itemArray = [];
        this.findAllBranchAndUser(data).then(response => {
          for (var [index, value] of Object.entries(response.data.data.objects)) {
            let item = {
                idDecrease: value.idDecrease,
                reason: value.reason,
                date: value.date,
                orderId: value.orderId,
                decreaseUser: value.decreaseUser
            };
            itemArray.push(item);
          }
        });
        return itemArray;
      }


}