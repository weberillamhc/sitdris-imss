/**
 * @author Ivo Danic Garrido
 * @company Erillam Healthcare
 * @version 1.0
 */

import WarehouseRequest from "./WarehouseRequest";
import ProductResquest from "./ProductRequest";
import OrderRequest from "./OrderRequest";
import FiscalFoundRequest from "./FiscalFoundRequest";
import LotRequest from "./LotRequest";
import LoginRequest from "./LoginRequest";
import CategoryRequest from "./CategoryRequest";
import FiscalFundRequest from "./FiscalFundRequest";
import ProductsRequest from "./ProductsRequest";
import SupplierRequest from "./SupplierRequest";
import JurisdictionRequest from "./JurisdictionRequest";
import PatientRequest from "./PatientRequest";
import AppointmentRequest from "./AppointmentRequest";
import BranchOfficeRequest from "./BranchOfficeRequest";
import UserRequest from "./UserRequest";
import ReceptionRequest from "./ReceptionRequest";
import IncidenceOrderRequest from "./IncidenceOrderRequest";
import RolRequest from "./RolRequest";
import ShipmentsRequest from "./ShipmentsRequest";
import ReturnRequest from "./ReturnRequest";
import StockRequest from "./StockRequest";
import DoctorRequest from "./DoctorRequest";
import MedicalPrescriptionRequest from "./MedicalPrescriptionRequest";
import DownProductRequest from "./DownProductRequest";
import ExcelRequest from "./ExcelRequest";
import DashboardRequest from "./DashboardRequest";
import PermissionRequest from "./PermissionRequest";
import RemmisionRequest from "./RemmisionRequest";
import ImageRequest from "./ImageRequest";

export {
    WarehouseRequest,
    ProductResquest,
    FiscalFoundRequest,
    OrderRequest,
    LotRequest,
    LoginRequest,
    CategoryRequest,
    FiscalFundRequest,
    ProductsRequest,
    SupplierRequest,
    JurisdictionRequest,
    PatientRequest,
    AppointmentRequest,
    BranchOfficeRequest,
    RolRequest,
    ReceptionRequest,
    IncidenceOrderRequest,
    UserRequest,
    ReturnRequest,
    ShipmentsRequest,
    StockRequest,
    DoctorRequest,
    MedicalPrescriptionRequest,
    DownProductRequest,
    ExcelRequest,
    DashboardRequest,
    PermissionRequest,
    ImageRequest,
    RemmisionRequest
};