/**
 * @author Alan Saucedo
 * @company Erillam Healthcare
 * @version 1.0
 */

import axios from "axios";
import { ServicePath } from "@/core";

export default class MedicalPrescriptionRequest {

    static addPrescription(data) {
      return axios.post(ServicePath.MEDICAL_PRESCRIPTION_ADD,data);
    }

    static findAllPrescriptionByBranchOffice(data) {
        return axios.post(ServicePath.MEDICAL_PRESCRIPTION_LIST_ALL_BY_BRANCH,data);
    }
   
        // console.log(MedicalPrescriptionArray);
      // for (var [index, value] of Object.entries(response.data.data.objects)) {
      //   let prescriptionItem = {
      //     orderId: value.orderId,
      //     prescriptionId: value.idPrescription,
      //     prescriptionKey: value.prescriptionKey,
      //     medicalRecord: value.medicalRecord,
      //     doctor: value.doctorName,
      //     doctorId: value.doctorId,
      //     prescriptionUser: value.user,
      //     date: value.date,
      //     branchOfficeName:value.branchOffice
      //   };
      //   MedicalPrescriptionArray.push(prescriptionItem);
      // }


    static findAllPrescriptionByBranchOfficeKey(data) {
        // let MedicalPrescriptionArray = [];
        // this.findAllPrescriptionByBranchOffice(data).then(response => {
        //     // console.log(response.data.data.object);
        //     MedicalPrescriptionArray = response.data.data.object.map((value)=>{
        //         return {
        //           orderId: value.orderId,
        //           prescriptionId: value.idPrescription,
        //           prescriptionKey: value.prescriptionKey,
        //           medicalRecord: value.medicalRecord,
        //           doctor: value.doctorName,
        //           doctorId: value.doctorId,
        //           prescriptionUser: value.user,
        //           date: value.date,
        //           branchOfficeName:value.branchOffice
        //         };
        //     });
        //     // console.log(MedicalPrescriptionArray);
        //   // for (var [index, value] of Object.entries(response.data.data.objects)) {
        //   //   let prescriptionItem = {
        //   //     orderId: value.orderId,
        //   //     prescriptionId: value.idPrescription,
        //   //     prescriptionKey: value.prescriptionKey,
        //   //     medicalRecord: value.medicalRecord,
        //   //     doctor: value.doctorName,
        //   //     doctorId: value.doctorId,
        //   //     prescriptionUser: value.user,
        //   //     date: value.date,
        //   //     branchOfficeName:value.branchOffice
        //   //   };
        //   //   MedicalPrescriptionArray.push(prescriptionItem);
        //   // }
        // });
        // return MedicalPrescriptionArray;
        let MedicalPrescriptionArray = [];
        this.findAllPrescriptionByBranchOffice(data)
        return this.findAllPrescriptionByBranchOffice(data);
    }


    static listAllPrescriptionByProduct(data) {
      return axios.post( ServicePath.MEDICAL_PRESCRIPTION_BY_PRODUCT , data );
    }

}