import Vue from "vue";

export default class Constant {


    static getDoctors() {
        return [
            {text:"Dr. Juan Díaz Perez", value:"Dr. Juan Díaz Perez"},
            {text:"Dra. Lucia Olvera Bravo", value:"Dra. Lucia Olvera Bravo"},
            // {text:"Dra. Karla Colín Bahena", value:"2058"},
            {text:"Dra. Ariadna Cruz Hernández", value:"Dr. Ariadna Cruz Hernández"},
            {text:"A quien corresponda", value:"A quien corresponda"},
          ];
    }
    static getSpecialities() {
        return [
            {text:"Especialidad 1", value:"Especialidad 1"},
            {text:"Especialidad 2", value:"Especialidad 2"},
            {text:"Especialidad 3", value:"Especialidad 3"},
            {text:"Otra", value:"Otra"},
          ];
    }

    static getTurns() {
        return [
            {text:"Matutino", value:"Matutino"},
            {text:"Vespertino", value:"Vespertino"},
            {text:"Nocturno", value:"Nocturno"},
            {text:"Mixto", value:"Mixto"},
          ];
    }
    static getShippingReasons() {
        return [
            {text:"Valoración", value:"Valoración"},
            {text:"Complicación", value:"Complicación"},
            {text:"Otro", value:"Otro"},
          ];
    }

    static getServiceOccasions() {
        return [
            {text:"Primera vez", value:"Primera vez"},
            {text:"Subsecuente", value:"Subsecuente"}
          ];
    }
    static getOfficeDoctors() {
        return [
            {text:"Consultorio 1", value:"Consultorio 1"},
            {text:"Consultorio 2", value:"Consultorio 2"},
            {text:"Otro", value:"Otro"},
          ];
    }

    static getCancellationReasons() {
        return [
            {text:"Cancelacion de Agenda", value:"Cancelacion de Agenda"},
            {text:"Cita Adelantada", value:"Cita Adelantada"},
            {text:"Datos errados en la asignación", value:"Datos errados en la asignación"},
            {text:"Fallas de infraestructura y/o equipos medicos", value:"Fallas de infraestructura y/o equipos medicos"},
            {text:"Otro", value:"Otro"},
          ];
    }
}
