export default class DTHeaders {
    /** Bidding Headers */
    static BIDDING = [
        { text: "ID Licitación", value: "idBidding" },
        { text: "Nombre Licitación", value: "name" },
        { text: "Referencia Licitación", value: "biddingKey" },
        { text: "Proveedores", value: "suppliersName", width: "30%"  },
        { text: "Fecha de Registro", value: "registrationDate" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static SUPPLIER = [
        { text: "ID Proveedor", value: "idSupplier" },
        { text: "Nombre Proveedor", value: "name" },
        { text: "RFC", value: "supplierKey" }
    ];
    static REMMISION = [
        { text: "ID Remisión", value: "idRemmision" },
        { text: "Remisión", value: "remissionkey" },
        { text: "Pedido", value: "requisitionKey" },
        { text: "Fecha de Registro", value: "dateInput" },
        { text: "Proveedores", value: "suppliersName" },
        { text: "Fondo Financiamiento", value: "fiscalFundKey" },
        { text: "Estatus", value: "status" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static REQUISITION = [
        { text: "ID Pedido", value: "idRequisitionGob" },
        { text: "Referencia Pedido", value: "requisitionKey" },
        { text: "Referncia Licitación", value: "biddingKey" },
        { text: "Nombre Licitación", value: "biddingName" },
        { text: "Fecha de Pedido", value: "dateRequisition" },
        { text: "Fondo Financiamiento", value: "fiscalFundKey" },
        { text: "Nombre Financiamiento", value: "fiscalFundName" },
        { text: "Estatus", value: "status" }
    ];
    static CREATE_REMMISION = [
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "name" },
        { text: "Presentación", value: "unit" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Lote", value: "lot" },
        { text: "Cantidad", value: "quantity" },
        { text: "Costo", value: "costo" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static CREATE_REMMISION_UPDATE = [
        { text: "ID Producto Remision", value: "idProductRemmison" },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "productDescription" },
        { text: "Presentación", value: "unitMeasurement" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Lote", value: "lot" },
        { text: "Cantidad", value: "quantity" },
        { text: "Costo", value: "unitprice" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static STOCK = [
        { text: "ID Stock", value: "idStock" },
        { text: "Ubicación", value: "locationKey" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Lote", value: "lote" },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "productName", width: "30%" },
        { text: "Financiamiento", value: "fiscalFund" },
        { text: "Cantidad", value: "quantity" },
        { text: "Estatus", value: "status" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static PRODUCT_STOCK = [
        { text: "ID Producto", value: "idProduct", align: ' d-none' },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "productName" },
        { text: "Cantidad Total", value: "quantity",width: "10%" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static STOCK_BY_PRODUCT = [
        { text: "ID Stock", value: "idStock" },
        { text: "Ubicación", value: "locationKey" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Lote", value: "lote" },
        { text: "Financiamiento", value: "fiscalFund" },
        { text: "Estatus", value: "status" },
        { text: "Cantidad Actual", value: "quantity" },
        { text: "Cantidad Nueva", value: "quantityNew", align: ' d-none'},
        { text: "Editar Cantidad", value: "actions", sortable: false, width: "20%" }
    ];



    static EXPIRED = [
        { text: "ID Stock", value: "idStock" },
        { text: "Ubicación", value: "locationKey" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Lote", value: "lote" },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "productFull", width: "30%" },
        { text: "Financiamiento", value: "fiscalFund" },
        { text: "Cantidad", value: "quantity" }
        // { text: "Tiempo a expirar", value: "monthsToExpireText" }
        // { text: "Acciones", value: "actions", sortable: false }
    ];
    static PERMISIONS_ROLE = [
        { text: "Permiso", value: "description" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static ORDERS = [
        { text: "destination id", value: "destinationId" , align: ' d-none' },
        //{ text: "id envio", value: "shipmentsId" , align: ' d-none'},
        { text: "status", value: "status" , align: ' d-none'},
        { text: "ID Salida", value: "orderId"},
        { text: "Usuario", value: "generate" },
        { text: "Destino", value: "destination" , width: "40%"  },
        { text: "Fecha Salida", value: "date" },
        { text: "Acciones", value: "actions", sortable: false  }
    ];
    static ORDER_PRODUCTS = [
        { text: "ID Stock", value: "idStock", align: ' d-none' },
        { text: "Fuente de Financiamiento", value: "fiscalFound" },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "product", width: "50%" },
        { text: "Lote", value: "lot" },
        { text: "Caducidad", value: "expirationDate" },
        //{ text: "Fecha Salida", value: "date" },
        { text: "Cantidad", value: "quantity" },
        { text: "Acciones", value: "actions", sortable: false  }
    ];

    static ORDER_PRODUCTS_RETURN = [
        { text: "ID Stock", value: "idStock" },
        { text: "Producto", value: "product", width: "50%" },
        { text: "Causes", value: "productKey" },
        { text: "Fuente de Financiamiento", value: "fiscalFound", width: "10%" },
        { text: "Lote", value: "lot" },
        { text: "Caducidad", value: "expirationDate" },
        { text: "Cantidad", value: "quantity" },
    ];

    /*static ORDER_PRODUCTS_PRESCRIPTION = [
        { text: "ID Stock", value: "idStock", align: ' d-none' },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "product", width: "50%" },
        { text: "Lote", value: "lot" },
        { text: "Caducidad", value: "expirationDate" },
        //{ text: "Fecha Salida", value: "date" },
        { text: "Cantidad", value: "quantity" },
        { text: "Recetados", value: "prescription" },
        { text: "Acciones", value: "actions", sortable: false  }
    ];*/
    static DISPENSING = [
        { text: "ID ORDEN", value: "orderId", align: ' d-none'},
        { text: "ID Receta", value: "idPrescription" },
        { text: "Usuario", value: "user" },
        { text: "Clave Receta", value: "prescriptionKey" },
        { text: "Benificiario", value: "medicalRecord" },
        { text: "ID Doctor", value: "doctorId", align: ' d-none'},
        { text: "Doctor", value: "doctorName"},
        { text: "Fecha de Surtimiento", value: "date" },
        { text: "Sucursal", value: "branchOfficeName" , align: ' d-none'},
        { text: "Acciones", value: "actions", sortable: false  }
    ];
    static DISPENSING_ALL_RECORDS = [
        { text: "ID ORDEN", value: "orderId", align: ' d-none'},
        { text: "ID Receta", value: "prescriptionId" },
        { text: "Sucursal", value: "branchOfficeName" },
        { text: "Usuario", value: "prescriptionUser" },
        { text: "Clave Receta", value: "prescriptionKey" },
        { text: "Benificiario", value: "medicalRecord" },
        { text: "ID Doctor", value: "doctorId", align: ' d-none'},
        { text: "Doctor", value: "doctor"},
        { text: "Fecha de Surtimiento", value: "date" },
        { text: "Acciones", value: "actions", sortable: false  }
    ];
    static CATEGORIES = [
        { text: "ID Categoría", value: "idCategory" },
        { text: "Nombre", value: "name" },
        { text: "Descripción", value: "description" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static FISCAL_FUND = [
        { text: "ID Fondo financiamiento", value: "idFiscalfund" },
        { text: "Siglas", value: "key" },
        { text: "Nombre", value: "name" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static PRODUCTS = [
        { text: "ID Producto", value: "idProduct" },
        { text: "Causes", value: "productKey" },
        { text: "Nombre", value: "name" },
        { text: "Unidad medida", value: "unit" },
        { text: "Categorías", value: "categories" },
        { text: "Descripción", value: "description" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static SUPPLIERS = [
        { text: "ID Proveedor", value: "idSupplier" },
        { text: "RFC", value: "supplierKey" },
        { text: "Nombre", value: "name" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static JURISDICTIONS = [
        { text: "ID jurisdicción", value: "idJurisdiction" },
        { text: "Nombre", value: "jurisdictionname" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static BRANCH_OFFICE = [
        { text: "ID Sucursal", value: "idBranchoffice" },
        { text: "CLUES", value: "branchOfficeKey" },
        { text: "ID Almacén", value: "warehouseId" },
        { text: "Nombre", value: "name" },
        { text: "Dirección", value: "fullAddress" },
        { text: "Jurisdicción", value: "jurisdiction.jurisdictionname" },
        { text: "Estatus", value: "status" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static USERS = [
        { text: "ID Usuario", value: "idUser" },
        { text: "Nombre Completo", value: "fullName" },
        { text: "Correo Electrónico", value: "email" },
        { text: "Rol", value: "roleName" },
        { text: "Estatus", value: "availableText" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static ROLES = [
        { text: "ID Rol", value: "idRole" },
        { text: "Nombre", value: "name" },
        { text: "Descripción", value: "description" },
        { text: "Cantidad Permisos", value: "totalPermmisions" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static PERMISSIONS = [
        { text: "ID Permiso", value: "idPermission" },
        { text: "Nombre", value: "name" },
        { text: "Descripción", value: "description" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static RECEPTION_PENDING = [
        { text: "ID Envío", value: "idShipment" },
        { text: "Fecha Envío", value: "dateshipment" },
        { text: "Origen", value: "fullNameOrigin" },
        { text: "Conductor", value: "driver" },
        { text: "Placa", value: "tag" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static SHIPMENTS = [
        { text: "conteo", value: "idCounter",  align: ' d-none' },
        { text: "Causes", value: "productKey" },
        { text: "Producto", value: "product" },
        { text: "Fecha de Caducidad", value: "expirationDate" },
        { text: "Ubicación", value: "locationKey" },
        { text: "Financiamiento", value: "fiscalFound" },
        { text: "Lote", value: "lot" },
        { text: "Cantidad", value: "quantity" },
        { text: "Acciones", vale: "actions", sortable: false} //Se agrego la pestaña de acciones para poder mandar a llamar el PDF
        //{ text: "Acciones", value: "actions", sortable: false }
    ];

    static PICKING = [
        { text: "ID Salida", value: "orderId" , width: "10%"},
        { text: "Destino", value: "destination" , width: "40%"},
        { text: "Clues", value: "destinationKey" },
        { text: "Fecha", value: "dateshipment" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static PICKING_MOV = [
        { text: "ID Salida", value: "orderId", width: "10%" },
        { text: "Destino", value: "destination", width: "20%" },
        { text: "Clues", value: "clues" },
        { text: "Responsable Picking", value: "picking" },
        { text: "Fecha Picking", value: "pickingDate" }, //Fecha de inicio de picking
        { text: "Responsable Validador", value: "validator" },
        { text: "Fecha Validador", value: "validatorDate" }, //Fecha de picking
    ];

    static SHIPMENTS_HISTORIC = [
        { text: "ID Envío", value: "idShipment" },
        { text: "Estatus de envío", value: "statusShipment" },
        { text: "ID Salida", value: "orderId" },
        { text: "Destino", value: "destination" },
        { text: "Clues", value: "destinationKey" },
        { text: "Fecha", value: "dateshipment" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static RECEPTION_HISTORY = [
        { text: "ID Recepción", value: "idReception" },
        { text: "ID Envío", value: "idShipment" },
        { text: "Fecha recepción", value: "dateReception" },
        { text: "Origen", value: "fullNameOrigin" },
        { text: "Estatus", value: "status" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static INCIDENCE_ORDER_IN = [
        { text: "ID incidencia", value: "idIncidence" },
        { text: "Razón", value: "reason", width: "30%" },
        { text: "Tipo", value: "typeIncidenceName"},
        { text: "Fecha incidencia", value: "dateIncidence" },
        { text: "Lugar incidencia", value: "fullNameOrigin" },
        { text: "Estatus", value: "statusName" },
        { text: "Acciones", value: "actions", sortable: false }
    ];
    static INCIDENCE_ORDER_OUT = [
        { text: "ID incidencia", value: "idIncidence" },
        { text: "Tipo", value: "typeIncidenceName"},
        { text: "Razón", value: "reason", width: "30%" },
        { text: "Fecha incidencia", value: "dateIncidence" },
        { text: "Proveedor", value: "supplierName" },
        { text: "Estatus", value: "statusName" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static DOCTOR = [
        { text: "ID Doctor", value: "idDoctor" },
        { text: "Nombre", value: "name"},
        { text: "Apellidos", value: "lastName"},
        { text: "Cedula", value: "professionalId" },
        { text: "Acciones", value: "actions", sortable: false }
    ];

    static DOWN_PRODUCT = [
        { text: "ID Merma", value: "idDecrease" },
        { text: "Usuario", value: "decreaseUser" },
        { text: "Motivo", value: "reason"},
        { text: "Fecha merma", value: "date"},
        //{ text: "ID Order", value: "orderId"},
        { text: "Acciones", value: "actions", sortable: false }
    ];


    static ORDERS_HISTORY = [
        { text: "ID Envío", value: "idShipment" },
        { text: "ID Order", value: "idOrder"},
        { text: "Generado por", value: "user" },
        { text: "Destino", value: "destination" , width: "40%"  },
        { text: "Fecha de Salida", value: "dateOrder" },
        { text: "Estatus Salida", value: "status"},
        { text: "Acciones", value: "actions", sortable: false  }
    ];

    static DISPENSING_HISTORY = [
        { text: "ID Receta", value: "idPrescription" },
        { text: "ID Order", value: "idOrder" },
        { text: "Usuario", value: "user" },
        { text: "Clave Receta", value: "prescriptionKey" },
        { text: "Benificiario", value: "medicalRecord" },
        { text: "Doctor", value: "doctor"},
        { text: "Fecha de Dispensación", value: "datePrescription" },
        { text: "Estatus", value: "status"},
        { text: "Acciones", value: "actions", sortable: false  }
    ];

    static DECREASE_HISTORY = [
        { text: "ID Merma", value: "idDecrease" },
        { text: "ID Order", value: "idOrder"},
        { text: "Usuario", value: "user" },
        { text: "Motivo", value: "reason"},
        { text: "Fecha de Merma", value: "dateDecrease"},
        { text: "Estatus", value: "status"},
        { text: "Acciones", value: "actions", sortable: false }
    ];


    static BRANCH_OFFICE_DASHBOARD = [
        { text: "Tag", value: "tag"},
        { text: "-", value: "status", width: "5%" },
        { text: "ID Sucursal", value: "idBranchoffice" ,  align: ' d-none'},
        { text: "Almacén", value: "name" },
        { text: "Clues", value: "branchOfficeKey"},
        { text: "ID Almacén", value: "warehouseId",  align: ' d-none'},
        { text: "Porcentaje", value: "value" },
        //{ text: "Dispensación", value: "quantityPrescription"},
        //{ text: "Json", value: "dataEntity",  align: ' d-none'},
        { text: "Excel", value: "actions", sortable: false },
    ];

    static PRODUCTS_DASHBOARD = [
        { text: "ID Producto", value: "idProduct" ,  align: ' d-none'},
        { text: "Causes", value: "productKey" },
        { text: "Descripcion", value: "description"},
        { text: "Lote", value: "lot"},
        { text: "Caducidad", value: "expirationDate"},
        { text: "Existencias", value: "quantity"},
        { text: "Dispensación", value: "quantityPrescription"},
        { text: "Claves Primer Nivel Autorizadas", value: "level", width: "15%" }
    ];

    static PRODUCTS_DASHBOARD_ACRE = [
        { text: "ID Producto", value: "idProduct" ,  align: ' d-none'},
        { text: "Causes", value: "productKey" },
        { text: "Descripcion", value: "description"},
        { text: "Lote", value: "lot"},
        { text: "Caducidad", value: "expirationDate"},
        { text: "Existencias", value: "quantity"},
        { text: "Dispensación", value: "quantityPrescription"},
        { text: "Claves de Acreditacion", value: "level", width: "15%" }
    ];

    static PRESCRIPTION_DASHBOARD = [
        { text: "ID Receta", value: "idPrescription" },
        { text: "Beneficiario", value: "medicalRecord" },
        { text: "Fecha Surtimiento", value: "datePrescription"},
        { text: "Curp", value: "curp"},
        { text: "Porcentaje de Surtimiento", value: "supplyPercentage"},

    ];


    static TRANSFER = [
        { text: "Origen", value: "branchOfficeNameOrigin" },
        { text: "Destino", value: "branchOfficeNameDestiny" },
        { text: "Causes", value: "productKey" },
        { text: "Descripción del Producto", value: "product" },
        { text: "Fecha de Transferencia ", value: "dateTransfer"},
        { text: "Cantidad Transferida", value: "quantity"},
        { text: "Transferido por", value: "userName"}
    ];

    static RETURNS = [
        { text: "ID Envio", value: "shipmentsId" },
        { text: "ID Salida", value: "orderId" },
        { text: "Usuario", value: "orderUser" },
        { text: "Destino", value: "destination" },
        { text: "Clues", value: "destinationKey" },
        { text: "Fecha de devolucion", value: "date" },
        { text: "Acciones", value: "actions", sortable: false },
    ];
    static PATIENTS = [
        { text: "ID Paciente", value: "idPatient" },
        { text: "Paciente", value: "fullNamePatient" },
        { text: "CURP", value: "curp" },
        { text: "Dirección", value: "fullAddress" },
        { text: "NSS", value: "imssNumber" },
        { text: "Agregado médico", value: "medicalAggregated" },
        { text: "Fecha registro", value: "registrationDate" },
        { text: "Acciones", value: "actions", sortable: false },
    ];


    static CONSULTATION = [
        // { text: "ID ORDEN", value: "orderId", align: ' d-none'},
        { text: "ID consulta", value: "idPrescription" },
        // { text: "Usuario", value: "user" },
        // { text: "Clave Receta", value: "prescriptionKey" },
        { text: "Paciente", value: "medicalRecord" },
        { text: "ID Doctor", value: "doctorId", align: ' d-none'},
        { text: "Doctor", value: "doctorName"},
        { text: "Fecha de consulta", value: "date" },
        // { text: "Sucursal", value: "branchOfficeName" , align: ' d-none'},
        { text: "Acciones", value: "actions", sortable: false  }
    ];

    static IMAGE = [
        { text: "ID estudio", value: "idStudy" },
        { text: "Paciente", value: "patient" },
        { text: "Modalidad", value: "modality"},
        { text: "Interprete", value: "interpreter" },
        { text: "Fecha de estudio", value: "date" },
        { text: "Estatus", value: "status" },
        { text: "Acciones", value: "actions", sortable: false  }
    ];






}