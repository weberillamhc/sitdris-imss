import Vue from "vue";

export default class UtilFront {

    static secretWord = "ERILLAMFMEDICAL2021EIFAJ\\*-*//";
    static keyDataUser = "14SDHVE*DSMJS*XDXADALV?$DEP--@PT4";

    static setIdUser(model) {
        model['idUser'] = this.getDataUser().idUser;
        return model;
    };

    static setUserID(model) {
        model['userID'] = this.getIdUser();
        return model;
    };

    static getIdUser() {
        return this.getDataUser().idUser;
    };
    
    static getDataUser(){
      return JSON.parse(this.getOfSession(this.keyDataUser));
    }

    static getPermissions(){
        return this.getDataUser().permissions;
      }
    static setDataUser(value){
        this.setOfSession(this.keyDataUser, JSON.stringify(value));
    }

    static getOfSession(key){
        return this.decrypt((new Vue).$session.get(key));
    }

    static setOfSession(key, value){
        (new Vue).$session.set(key, this.encrypt(value));
    }
    static getLoadding(){
        return (new Vue).$loading.show({
            canCancel: false, 
            color: '#00C337',
            loader: 'bars',
            width: 200,
            height: 200,
            backgroundColor: '#ffffff',
            // opacity: 0.5,
            zIndex: 999,
          });
    }

    static encrypt(valueNoEncryped){
        const encryptedText = (new Vue).CryptoJS.AES.encrypt(valueNoEncryped, this.secretWord).toString();
        return encryptedText;
    }

    static decrypt(valueEncrypted){
        const decryptedText = (new Vue).CryptoJS.AES.decrypt(valueEncrypted, this.secretWord).toString((new Vue).CryptoJS.enc.Utf8);
        return decryptedText;
    }

    static withPermission(role){
        let currentRole = this.getDataUser().role.name;
        let nameRole = "";
        switch (role) {
            case 1:
                nameRole = "Administrador Operativo"
                break;
            case 2:
                nameRole = "Administrador Consulta"
                break;
            case 3:
                nameRole = "Capturista";
                break;
            case 4:
                nameRole = "Capturista";
                break;
            case 5:
                nameRole = "Administracion"
                break;
            default:
                break;
        }
        return nameRole.trim().toLocaleLowerCase() == currentRole.trim().toLocaleLowerCase();
    }
    static withPermissionName(permissionName){
        let withPermission = true;
        // try {
        //     withPermission = this.getPermissions().filter((perm)=>{
        //         return permissionName.trim().toLocaleLowerCase() == perm.name.trim().toLocaleLowerCase()
        //     }).length > 0;
        //     withPermission = withPermission || this.withPermission(1);
        // } catch (error) {
        //     console.log(error);
        // }

        return withPermission;
        
    }

    static getMessageError(data){
        let {entry} = data;
        if(entry){
            let {response} = entry[0];
            if(response){
                if(response.status == "201 OK" || response.status == "201"){
                    return null;
                }
                if(response.outcome != null){
                    if(response.outcome.issue != null){
                        if(response.outcome.issue.details != null){
                            if(response.outcome.issue.details.text != null){
                                return response.outcome.issue.details.text;
                            }
                        }
                    }
                }
            }
           
        }
        
        return "Ocurrio un error interno";
        
    }
}