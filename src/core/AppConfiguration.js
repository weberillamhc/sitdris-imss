/**
 *
 * @author Ivo Danic Garrido
 * @company  Erillam Healthcare
 * @version 1.0
 */

const AppConfiguration = {
    // WEB_SERVICE_HOST:  process.env.NODE_ENV === 'production' ? window.location.hostname : "localhost" ,
    WEB_SERVICE_HOST:  process.env.NODE_ENV === 'production' ? window.location.hostname : "187.188.98.157" ,
    WEB_SERVICE_PORT: process.env.NODE_ENV === 'production' ? ":8080" : ":8210",
    WEB_SERVICE_PROTOCOL: "http://",
    WEB_SERVICE_UNSECURE_PATH: process.env.NODE_ENV === 'production' ? "/whmws/app/" :  "/whmws/app/" ,
    WEB_SERVICE_SECURE_PATH: process.env.NODE_ENV === 'production' ? "/whmws/app/sc/" :  "/whmws/app/sc/" ,
};

export const SECURE_ENDPOINT = AppConfiguration.WEB_SERVICE_PROTOCOL.concat(
    AppConfiguration.WEB_SERVICE_HOST,
    AppConfiguration.WEB_SERVICE_PORT,
    AppConfiguration.WEB_SERVICE_SECURE_PATH
);
export const UNSECURE_ENDPOINT = AppConfiguration.WEB_SERVICE_PROTOCOL.concat(
    AppConfiguration.WEB_SERVICE_HOST,
    AppConfiguration.WEB_SERVICE_PORT,
    AppConfiguration.WEB_SERVICE_UNSECURE_PATH
);