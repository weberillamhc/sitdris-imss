/**
 * @author Ivo Danic Garrido
 * @company Erillam Healthcare
 * @version 1.0
 */

import Topbar from "./Topbar.vue";
import Sidebar from "./SidebarMenuComponent.vue";
import Footer from "./FooterComponent.vue";
import Datatable from "./Datatable.vue";


export { Topbar, Sidebar, Footer, Datatable };
